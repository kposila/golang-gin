package main

import (
	"fmt"
	"net/http"

	"github.com/caarlos0/env"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

type config struct {
	Port int    `env:"PORT" envDefault:"3000"`
	Mode string `env:"GIN_MODE" envDefault:"debug"`
}

func main() {
	// Configuration
	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	gin.SetMode(cfg.Mode)

	// Set the router as the default one shipped with Gin
	router := gin.Default()

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})
	}

	// Start and run the server
	router.Run(fmt.Sprintf(":%d", cfg.Port))
}
