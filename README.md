# golang-gin

Based on this [tutorial](https://medium.freecodecamp.org/how-to-build-a-web-app-with-go-gin-and-react-cffdc473576).

## Configuration

Application can be configured via environment properties:

| Env  | Default | Description               |
|------|---------|---------------------------|
| PORT | 3000    | Port for server to listen |
| GIN_MODE| debug | Setup mode of Gin server|
| | |`debug`: Server will print debug iformation|
| | |`production`: Production mode|

## Running

Simply run main.go:

```bash
go build main.go
```